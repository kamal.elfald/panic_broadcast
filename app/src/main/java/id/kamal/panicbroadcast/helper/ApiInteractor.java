package id.kamal.panicbroadcast.helper;

import id.kamal.panicbroadcast.model.JenisBahayaModel;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by kamal on 30/11/16.
 */

public interface ApiInteractor {
    Observable<JenisBahayaModel> getJenisBahaya();
}
