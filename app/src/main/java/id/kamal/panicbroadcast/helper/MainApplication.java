package id.kamal.panicbroadcast.helper;

import android.app.Application;
import android.content.Context;

import com.mapbox.mapboxsdk.MapboxAccountManager;

/**
 * Created by kamal on 24/11/16.
 */

public class MainApplication extends Application {

    //public final static String MAIN_URL = "http://192.168.43.56/sos/";
    //public final static String MAIN_URL = "http://192.168.1.72/sos/";
    public final static String MAIN_URL = "http://panicbroadcast.inovativo.id/";
    public final static String LOGIN = MAIN_URL+"user/login";
    public final static String REGISTER = MAIN_URL+"user/register";
    public final static String JENIS_BAHAYA = MAIN_URL+"user/get-jenis-bahaya";
    public final static String UPDATE_LOKASI = MAIN_URL+"user/update-location";
    public final static String UPDATE_USER = MAIN_URL+"user/update-user";
    public final static String PROFILE = MAIN_URL+"user/profile";
    public final static String GET_BROADCAST = MAIN_URL+"broadcast/get-broadcast";
    public final static String BROADCAST = MAIN_URL+"broadcast/broadcast";
    public final static String GET_BROADCAST_RECEIVED = MAIN_URL+"broadcast/get-broadcast-receiver";
    public final static String BROADCAST_VOTE = MAIN_URL+"broadcast/vote";
    public final static String GET_STATUS_VOTE = MAIN_URL+"broadcast/get-status-vote";
    public final static String STOP_BROADCAST = MAIN_URL+"broadcast/stop-broadcast";
    public final static String GET_NEARBY = MAIN_URL+"broadcast/get-nearby";
    public final static String GET_STATUS_BROADCAST = MAIN_URL+"broadcast/get-status-broadcast";

    public final static String TOKEN_MAPBOX = "pk.eyJ1IjoiZWxmYWRsIiwiYSI6ImNpdDdmNjViZjA4cXEyb3AxaWgzbnRjODAifQ.c19mJ3heD-tRpxRyAtZzQg";

    public static final String PUSH_NOTIFICATION = "pushNotification";

    private static Context context;
    CustomFontFamily customFontFamily;

    @Override
    public void onCreate() {
        super.onCreate();
        MainApplication.context=this;
        customFontFamily=CustomFontFamily.getInstance();
        // add your custom fonts here with your own custom name.
        customFontFamily.addFont("bold","Raleway-Bold.otf");
        customFontFamily.addFont("regular","Raleway-Regular.otf");
        MapboxAccountManager.start(this, TOKEN_MAPBOX);
        PrefMgr.getInstance().init(this);
    }
    public static Context getContext() {
        return context;
    }

}
