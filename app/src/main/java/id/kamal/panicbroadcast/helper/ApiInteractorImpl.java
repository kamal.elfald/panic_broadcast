package id.kamal.panicbroadcast.helper;

import id.kamal.panicbroadcast.helper.api.RetrofitApi;
import id.kamal.panicbroadcast.model.JenisBahayaModel;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by kamal on 30/11/16.
 */

public class ApiInteractorImpl implements ApiInteractor {

    private RetrofitApi apiServices;

    public ApiInteractorImpl() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MainApplication.MAIN_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        apiServices = retrofit.create(RetrofitApi.class);
    }

    @Override
    public Observable<JenisBahayaModel> getJenisBahaya() {
        return apiServices.getJenisBahaya().subscribeOn(Schedulers.io());
    }
}
