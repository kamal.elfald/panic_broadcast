package id.kamal.panicbroadcast.helper;

import android.content.res.ColorStateList;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

/**
 * Created by kamal on 09/12/16.
 */

public class CustomSnackbar {
    private View view;
    private String title;
    private int duration;
    private View.OnClickListener listener;
    private String titleListener;
    private Snackbar snackbar;
    private TextView tv;

    public CustomSnackbar(View view, String title, int duration, String titleListener, View.OnClickListener listener) {
        this.view = view;
        this.title = title;
        this.duration = duration;
        this.listener = listener;
        this.titleListener = titleListener;
        snackbar = Snackbar.make(view, title, duration).setAction(titleListener, listener);
        tv = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
    }

    public CustomSnackbar(View view, String title, int duration) {
        this.view = view;
        this.title = title;
        this.duration = duration;
        snackbar = Snackbar.make(view, title, duration);
        tv = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
    }

    public CustomSnackbar setTextColor(ColorStateList color){
        tv.setTextColor(color);
        return this;
    }

    public CustomSnackbar setTextColor(int color){
        tv.setTextColor(color);
        return this;
    }

    public TextView getTv() {
        return tv;
    }

    public void show(){
        snackbar.show();
    }
}
