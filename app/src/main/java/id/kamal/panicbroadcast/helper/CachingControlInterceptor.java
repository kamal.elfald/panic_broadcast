package id.kamal.panicbroadcast.helper;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by kamal on 30/11/16.
 */

public class CachingControlInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        if(request.method().equals("GET")){
            if(Connectivity.isConnected(MainApplication.getContext())){
                request = request.newBuilder()
                        .header("Cache-Control", "only-if-cached")
                        .build();
            }else {
                request = request.newBuilder()
                        .header("Cache-Control", "public, max-stale=2419300")
                        .build();
            }
        }

        Response originalResponse = chain.proceed(request);
        return originalResponse.newBuilder()
                .header("Cache-Control", "max-age=600")
                .build();
    }
}
