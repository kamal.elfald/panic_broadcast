package id.kamal.panicbroadcast.helper.api;

import id.kamal.panicbroadcast.helper.MainApplication;
import id.kamal.panicbroadcast.model.BroadcastModel;
import id.kamal.panicbroadcast.model.BroadcastReceivedModel;
import id.kamal.panicbroadcast.model.GetBroadcastModel;
import id.kamal.panicbroadcast.model.GetStatusBroadcastModel;
import id.kamal.panicbroadcast.model.JenisBahayaModel;
import id.kamal.panicbroadcast.model.LoginModel;
import id.kamal.panicbroadcast.model.DefaultModel;
import id.kamal.panicbroadcast.model.NearbyModel;
import id.kamal.panicbroadcast.model.ProfileModel;
import id.kamal.panicbroadcast.model.StatusVoteModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by elmee on 09/03/2016.
 */
public interface RetrofitApi {

    @FormUrlEncoded
    @POST(MainApplication.LOGIN)
    Call<LoginModel> login(@Field("username") String username, @Field("password") String password, @Field("token") String token);

    @FormUrlEncoded
    @POST(MainApplication.REGISTER)
    Call<DefaultModel> register(@Field("username") String username, @Field("email") String email, @Field("password") String password, @Field("nama") String nama, @Field("alamat") String alamat, @Field("tgl_lahir") String tglLahir, @Field("gender") int gender, @Field("token") String token);

    @GET(MainApplication.JENIS_BAHAYA)
    Observable<JenisBahayaModel> getJenisBahaya();

    @FormUrlEncoded
    @POST(MainApplication.UPDATE_LOKASI)
    Call<DefaultModel> updateLocation(@Field("token") String token, @Field("latitude") String latitude, @Field("longitude") String longitude);

    @FormUrlEncoded
    @POST(MainApplication.UPDATE_USER)
    Call<DefaultModel> updateUser(@Field("token") String token, @Field("token_firebase") String tokenFirebase);

    @GET(MainApplication.PROFILE)
    Call<ProfileModel> getProfile(@Query("token") String token);

    @GET(MainApplication.GET_BROADCAST)
    Call<GetBroadcastModel> getBroadcast(@Query("token") String token);

    @FormUrlEncoded
    @POST(MainApplication.BROADCAST)
    Call<BroadcastModel> broadcast(@Field("token") String token, @Field("lat") double lat, @Field("lng") double lng, @Field("radius") String radius, @Field("jenis_bahaya") String jenisBahaya, @Field("status") String status);

    @GET(MainApplication.GET_BROADCAST_RECEIVED)
    Call<BroadcastReceivedModel> getBroadcastReceived(@Query("token") String token);

    @FormUrlEncoded
    @POST(MainApplication.BROADCAST_VOTE)
    Call<DefaultModel> broadcastVote(@Field("token") String token, @Field("id_broadcast") String idBroadcast, @Field("vote") int vote);

    @FormUrlEncoded
    @POST(MainApplication.GET_STATUS_VOTE)
    Call<StatusVoteModel> getStatusVote(@Field("token") String token, @Field("id_broadcast") String idBroadcast);

    @FormUrlEncoded
    @POST(MainApplication.STOP_BROADCAST)
    Call<DefaultModel> stopBroadcast(@Field("token") String token, @Field("id_broadcast") String idBroadcast);

    @FormUrlEncoded
    @POST(MainApplication.GET_NEARBY)
    Call<NearbyModel> getNearby(@Field("token") String token, @Field("lat") double lat, @Field("lng") double lng, @Field("radius") String radius);

    @FormUrlEncoded
    @POST(MainApplication.GET_STATUS_VOTE)
    Call<GetStatusBroadcastModel> getStatusBroadcast(@Field("token") String token);

    /*

    @FormUrlEncoded
    @POST(MainApplication.URL_DAFTAR)
    Call<RegisterViewModel> register(@Field("token") String token, @Field("nik") String nik, @Field("nama") String nama, @Field("email") String email, @Field("no_hp") String noHP, @Field("img_code") String img, @Field("filename") String filename, @Field("pin") String pin);

    @FormUrlEncoded
    @POST(MainApplication.URL_LOGIN)
    Call<LoginViewModel> login(@Field("nik") String nik, @Field("pin") String pin, @Field("token") String token);

    @FormUrlEncoded
    @POST(MainApplication.URL_EDIT_PROFIL)
    Call<DefaultReturn> editProfil(@Field("alamat") String alamat, @Field("no_hp") String noHP, @Field("tempat_lahir") String tempatLahir, @Field("tanggal_lahir") String tanggalLahir, @Field("pekerjaan") String pekerjaan, @Field("jenis_kelamin") String jenisKelamin, @Field("token") String token);

    @GET(MainApplication.URL_JENIS_LAPORAN)
    Call<JenisLaporan> getJenisLaporan();

    @GET(MainApplication.URL_PANWAS)
    Call<InformasiPengawasanModel> getPanwas();

    @GET(MainApplication.URL_BERITA)
    Call<DataBerita> getBerita();

    @Multipart
    @POST(MainApplication.URL_LAPORAN)
    Call<DefaultReturn> laporan(@Query("token") String token, @Query("uraian") String uraian, @Query("lokasi") String lokasi, @Query("kecamatan") String kecamatan, @Query("kelurahan") String kelurahan, @Query("tanggal_kejadian") String tanggalKejadian, @Query("jenis_laporan") String jenisLaporan, @Query("nama_terlapor") String namaTerlapor, @Query("alamat_terlapor") String alamatTerlapor, @Query("no_terlapor") String noTelepon, @Query("saksi") String saksi, @Query("bukti") String bukti, @Part List<MultipartBody.Part> files, @Query("isi") int isi);

    @GET(MainApplication.URL_GET_LAPORAN)
    Call<GetLaporan> getLaporan(@Query("token") String token);

    @GET(MainApplication.URL_PASLON)
    Call<ModelPaslon> paslon();

    @GET(MainApplication.URL_TUPOKSI)
    Call<Tupoksi> tupoksi();

    @FormUrlEncoded
    @POST(MainApplication.URL_FIREBASEID)
    Call<DefaultReturn> firebaseID(@Field("token") String token, @Field("firebaseid") String id);

    @FormUrlEncoded
    @POST(MainApplication.URL_GET_IMG)
    Call<GetImage> getImage(@Field("image") String image);

    @GET(MainApplication.URL_NOTIFIKASI)
    Call<PemberitahuanModel> getNotif(@Query("id") String idMember, @Query("token") String token);

    @GET(MainApplication.URL_GET_SINGLE_LAPORAN)
    Call<SingleLaporanModel> getSingleLaporan(@Query("id") String idLaporan);

    @GET(MainApplication.URL_UPDATE_STATUS_PESAN)
    Call<DefaultReturn> updateStatusPesan(@Query("id") String idPesan, @Query("token") String token);

    @GET(MainApplication.URL_VERIFIKASI)
    Call<Verifikasi> verifikasi(@Query("token") String token);*/

}