package id.kamal.panicbroadcast.helper.api;

import id.kamal.panicbroadcast.helper.CachingControlInterceptor;
import id.kamal.panicbroadcast.helper.MainApplication;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by elmee on 09/03/2016.
 */
public class RetrofitApiSingleton {

    private static RetrofitApiSingleton singleton;
    private RetrofitApi retrofitApi;

    public static RetrofitApiSingleton getInstance(){
        if(null == singleton){
            singleton = new RetrofitApiSingleton();
        }
        return singleton;
    }

    public void init(){

        //MainApplication apps = new MainApplication();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        //httpClient.networkInterceptors().add(new CachingControlInterceptor());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MainApplication.MAIN_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        retrofitApi = retrofit.create(RetrofitApi.class);
    }

    public RetrofitApi getApi(){
        return retrofitApi;
    }

}
