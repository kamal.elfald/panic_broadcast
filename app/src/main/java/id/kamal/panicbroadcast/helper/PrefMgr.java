package id.kamal.panicbroadcast.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by elmee on 09/03/2016.
 */
public class PrefMgr {

    public static final String TOKEN_FIREBASE = "tfb";
    public static final String TOKEN_FACEBOOK = "tFB";
    public static final String ID_USER = "idsr";
    public static final String STATUS_TOKEN = "ststkn";
    public static final String STATUS_BROADCAST = "stsbrcst";
    public static final String BROADCAST_ID = "brcstid";
    public static final String BROADCAST_JENIS_BAHAYA = "brcstjnsbhy";
    public static final String BROADCAST_RADIUS = "brcstrds";
    public static final String BROADCAST_LATITUDE = "brcstlat";
    public static final String BROADCAST_LONGITUDE = "brcstlng";

    private static PrefMgr singleton;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public static PrefMgr getInstance(){
        if(singleton == null){
            singleton = new PrefMgr();
        }
        return singleton;
    }

    public void init(Context context){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();
    }

    public void savePreference(String key, Object object){
        if(object instanceof String){
            editor.putString(key, (String) object);
        }else if(object instanceof Boolean){
            editor.putBoolean(key, (Boolean) object);
        }
        editor.commit();
    }

    public void removeKey(String key){
        editor.remove(key);
        editor.apply();
    }

    public boolean isExist(String s){
        return sharedPreferences.contains(s);
    }

    public void saveTokenFirebase(String token){
        savePreference(TOKEN_FIREBASE, token);
    }

    public String getTokenFirebase(){
        return sharedPreferences.getString(TOKEN_FIREBASE, null);
    }

    public void saveTokenFacebook(String token){
        savePreference(TOKEN_FACEBOOK, token);
    }

    public String getTokenFacebook(){
        return sharedPreferences.getString(TOKEN_FACEBOOK, null);
    }

    public void saveIdUser(String iduser){
        savePreference(ID_USER, iduser);
    }

    public String getIdUser() {
        return sharedPreferences.getString(ID_USER, null);
    }

    public void saveStatusToken(boolean status){
        savePreference(STATUS_TOKEN, status);
    }

    public boolean isTokenSend(){
        return sharedPreferences.getBoolean(STATUS_TOKEN, false);
    }

    public void saveStatusBroadcast(boolean status){
        savePreference(STATUS_BROADCAST, status);
    }

    public boolean isBroadcasting(){
        return sharedPreferences.getBoolean(STATUS_BROADCAST, false);
    }

    public void saveBroadcastId(String broadcastId){
        savePreference(BROADCAST_ID, broadcastId);
    }

    public String getBroadcastId() {
        return sharedPreferences.getString(BROADCAST_ID, null);
    }

    public void saveBroadcastJenisBahaya(String broadcastJenisBahaya){
        savePreference(BROADCAST_JENIS_BAHAYA, broadcastJenisBahaya);
    }

    public String getBroadcastJenisBahaya() {
        return sharedPreferences.getString(BROADCAST_JENIS_BAHAYA, null);
    }

    public void saveBroadcastRadius(String broadcastRadius){
        savePreference(BROADCAST_RADIUS, broadcastRadius);
    }

    public String getBroadcastRadius() {
        return sharedPreferences.getString(BROADCAST_RADIUS, null);
    }

    public void saveBroadcastLat(String broadcastLatitude){
        savePreference(BROADCAST_LATITUDE, broadcastLatitude);
    }

    public String getBroadcastLatitude() {
        return sharedPreferences.getString(BROADCAST_LATITUDE, null);
    }

    public void saveBroadcastLongitude(String broadcastLongitude){
        savePreference(BROADCAST_LONGITUDE, broadcastLongitude);
    }

    public String getBroadcastLongitude() {
        return sharedPreferences.getString(BROADCAST_LONGITUDE, null);
    }
}
