package id.kamal.panicbroadcast;

import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import id.kamal.panicbroadcast.databinding.LoginBinding;
import id.kamal.panicbroadcast.viewmodel.LoginViewModel;

/**
 * Created by kamal on 24/11/16.
 */

public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
        super.onCreate(savedInstanceState);
        LoginBinding binding = DataBindingUtil.setContentView(this, R.layout.login);
        LoginViewModel login = new LoginViewModel();
        binding.setLogin(login);
    }
}
