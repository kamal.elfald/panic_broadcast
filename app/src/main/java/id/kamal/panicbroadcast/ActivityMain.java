package id.kamal.panicbroadcast;

import android.*;
import android.Manifest;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import java.util.ArrayList;
import java.util.List;

import id.kamal.panicbroadcast.databinding.MainLayoutBinding;
import id.kamal.panicbroadcast.helper.PrefMgr;
import id.kamal.panicbroadcast.helper.api.RetrofitApi;
import id.kamal.panicbroadcast.helper.api.RetrofitApiSingleton;
import id.kamal.panicbroadcast.model.DefaultModel;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kamal on 29/11/16.
 */

public class ActivityMain extends AppCompatActivity implements EasyPermissions.PermissionCallbacks{
    //private Toolbar toolbar;
    //BottomNavigationView bottomNavigationView;
    PrefMgr prefMgr;
    int count = 0;
    FragmentTransaction ft;
    ViewPagerAdapter adapter;
    MainLayoutBinding binding;
    private static final int REQUEST_PERMISSION_CODE = 892;
    String[] perm = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.main_layout);
        binding.title.setText("broadcast");

        prefMgr = PrefMgr.getInstance();
        if(!prefMgr.isTokenSend()){
            sendRegistrationToServer(prefMgr.getIdUser(), prefMgr.getTokenFirebase());
        }

        /*toolbar = (Toolbar) findViewById(R.id.toolbar);*/
        binding.toolbar.setOverflowIcon(ContextCompat.getDrawable(this,R.drawable.ic_more));
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setupViewPager(binding.viewpager);
        binding.viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //invalidateFragmentMenus(position);
                //invalidateOptionsMenu();
                switch (position){
                    case 0:
                        binding.title.setText("broadcast");
                        break;
                    case 1:
                        binding.title.setText("nearby");
                        break;
                    case 2:
                        binding.title.setText("profile");
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        binding.tabs.setupWithViewPager(binding.viewpager);
        setupTabIcons();

        /*bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(new OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                switch (item.getItemId()){
                    case R.id.menu_broadcast:
                        ft.replace(R.id.container, new Broadcast());
                        break;
                    case R.id.menu_nearby:
                        ft.replace(R.id.container, new Broadcast());
                        break;
                    case R.id.menu_profile:
                        ft.replace(R.id.container, new Broadcast());
                        break;
                }
                ft.commit();
                return false;
            }
        });*/
        /*bottomBar = (BottomBar) findViewById(R.id.bottomBar);

        ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.container, new Broadcast());
        ft.commit();

        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                switch (tabId){
                    case R.id.menu_broadcast:
                        FragmentTransaction ft = ActivityMain.this.getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.container, new Broadcast());
                        ft.commit();
                        break;
                    case R.id.menu_nearby:
                        FragmentTransaction ft2 = ActivityMain.this.getSupportFragmentManager().beginTransaction();
                        ft2.replace(R.id.container, new Broadcast());
                        ft2.commit();
                        break;
                    case R.id.menu_profile:
                        FragmentTransaction ft3 = ActivityMain.this.getSupportFragmentManager().beginTransaction();
                        ft3.replace(R.id.container, new Profile());
                        ft3.commit();
                        break;
                }
            }
        });*/

    }

    private void invalidateFragmentMenus(int position){
        for (int i = 0; i < adapter.getCount(); i++) {
            adapter.getItem(i).setHasOptionsMenu(i == position);
        }
        invalidateOptionsMenu();
    }

    private void sendRegistrationToServer(String tokenUser, String refreshedToken){
        RetrofitApi api;
        RetrofitApiSingleton.getInstance().init();
        api = RetrofitApiSingleton.getInstance().getApi();

        Call<DefaultModel> updateUser = api.updateUser(tokenUser, refreshedToken);
        updateUser.enqueue(new Callback<DefaultModel>() {
            @Override
            public void onResponse(Call<DefaultModel> call, Response<DefaultModel> response) {
                count = 0;
                DefaultModel result = response.body();
                Log.i("SEND TOKEN", result.getMessage());
                prefMgr.saveStatusToken(true);
            }

            @Override
            public void onFailure(Call<DefaultModel> call, Throwable t) {
                if(count < 5){
                    count++;
                    call.clone().enqueue(this);
                }else {
                    Log.i("SEND TOKEN", "GAGAL");
                    count = 0;
                }
            }
        });
    }

    private void setupTabIcons() {
        /*TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tabs, null);
        tabOne.setText("Broadcast");
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, android.R.drawable.ic_lock_silent_mode_off, 0, 0);*/
        binding.tabs.getTabAt(0).setIcon(R.drawable.ic_broadcast);

        /*TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tabs, null);
        tabTwo.setText("Nearby");
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, android.R.drawable.ic_menu_mylocation, 0, 0);*/
        binding.tabs.getTabAt(1).setIcon(R.drawable.ic_nearby);

        /*TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tabs, null);
        tabThree.setText("Profile");
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, android.R.drawable.ic_menu_myplaces, 0, 0);*/
        binding.tabs.getTabAt(2).setIcon(R.drawable.ic_profile);
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentContainer(), "Broadcast");
        adapter.addFragment(new Nearby(), "Nearby");
        adapter.addFragment(new Profile(), "Profile");
        viewPager.setAdapter(adapter);
        //invalidateFragmentMenus(viewPager.getCurrentItem());
        //invalidateOptionsMenu();
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if(EasyPermissions.somePermissionPermanentlyDenied(this, perms)){
            new AppSettingsDialog.Builder(this, "We need access your location to get your valid position in the map and also need your internet connection to connect to the server")
                    .setTitle("Permissions")
                    .setPositiveButton("Allow")
                    .setNegativeButton("Deny", null)
                    .setRequestCode(REQUEST_PERMISSION_CODE)
                    .build().show();
        }
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.add(0, 1, 1, "Logout");
        /*if(binding.viewpager.getCurrentItem() == 0){
            menu.findItem(R.id.logout).setVisible(true);
        }else {
            menu.findItem(R.id.logout).setVisible(false);
        }*/
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case 1:
                PrefMgr prefMgr = PrefMgr.getInstance();
                prefMgr.removeKey(PrefMgr.ID_USER);
                startActivity(new Intent(this, Login.class));
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @AfterPermissionGranted(REQUEST_PERMISSION_CODE)
    private void methodRequestPermission(){
        if(!EasyPermissions.hasPermissions(this, perm)){
            EasyPermissions.requestPermissions(this, "We need access your location to get your valid position in the map and also need your internet connection to connect to the server", REQUEST_PERMISSION_CODE, perm);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }
}
