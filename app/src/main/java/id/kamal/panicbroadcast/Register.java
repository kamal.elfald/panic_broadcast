package id.kamal.panicbroadcast;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import id.kamal.panicbroadcast.databinding.RegisterBinding;
import id.kamal.panicbroadcast.helper.CustomSnackbar;
import id.kamal.panicbroadcast.helper.PrefMgr;
import id.kamal.panicbroadcast.helper.api.RetrofitApi;
import id.kamal.panicbroadcast.helper.api.RetrofitApiSingleton;
import id.kamal.panicbroadcast.model.DefaultModel;
import id.kamal.panicbroadcast.viewmodel.RegisterViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.basgeekball.awesomevalidation.ValidationStyle.BASIC;

/**
 * Created by kamal on 24/11/16.
 */

public class Register extends AppCompatActivity {

    RegisterBinding binding;
    RegisterViewModel register;
    int count = 0;
    PrefMgr pref;
    String strTanggal = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.register);
        register = new RegisterViewModel();
        binding.setRegister(register);

        binding.birthDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                strTanggal = String.valueOf(year)+"-"+String.valueOf(monthOfYear+1)+"-"+String.valueOf(dayOfMonth);
                                String tanggal = "Birthday : "+String.valueOf(dayOfMonth)+"-"+String.valueOf(monthOfYear+1)+"-"+String.valueOf(year);
                                binding.birthDay.setText(tanggal);
                            }
                        },
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                /*Calendar maxDate = Calendar.getInstance();
                maxDate.set(now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH)+7);*/
                //dpd.setMinDate(now);
                Calendar maxDate = Calendar.getInstance();
                dpd.setMaxDate(maxDate);
                dpd.setAccentColor(ContextCompat.getColor(Register.this, R.color.colorAccent));
                dpd.show(Register.this.getFragmentManager(), "Pilih Tanggal");
            }
        });

        binding.btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AwesomeValidation mAwesomeValidation = new AwesomeValidation(BASIC);

                mAwesomeValidation.addValidation(binding.fullname, RegexTemplate.NOT_EMPTY, "Full Name tidak boleh kosong");
                mAwesomeValidation.addValidation(binding.username, RegexTemplate.NOT_EMPTY, "Username tidak boleh kosong");
                mAwesomeValidation.addValidation(binding.password, RegexTemplate.NOT_EMPTY, "Password tidak valid");
                mAwesomeValidation.addValidation(binding.repassword,RegexTemplate.NOT_EMPTY, "Re-type Password tidak valid");
                mAwesomeValidation.addValidation(binding.email, android.util.Patterns.EMAIL_ADDRESS, "Email tidak valid");
                mAwesomeValidation.addValidation(binding.address, RegexTemplate.NOT_EMPTY, "Address tidak boleh kosong");
                //mAwesomeValidation.addValidation(edtPassword, RegexTemplate.NOT_EMPTY, getString(R.string.err_pass));
                if(mAwesomeValidation.validate()){
                    int gender = binding.male.isChecked() ? 1 : binding.female.isChecked() ? 0 : -1;
                    if(gender != -1 && strTanggal.length() > 0) {
                        if(binding.password.getText().toString().length() > 5) {
                            if (binding.password.getText().toString().equals(binding.repassword.getText().toString())) {
                                RetrofitApiSingleton.getInstance().init();
                                RetrofitApi api = RetrofitApiSingleton.getInstance().getApi();
                                pref = PrefMgr.getInstance();
                                pref.init(Register.this);

                                register.setLoading(true);

                                Call<DefaultModel> registrasi = api.register(
                                        register.getUsername(),
                                        register.getEmail(),
                                        register.getPassword(),
                                        register.getFullname(),
                                        register.getAddress(),
                                        strTanggal,
                                        gender,
                                        pref.getTokenFirebase()
                                );
                                registrasi.enqueue(new Callback<DefaultModel>() {
                                    @Override
                                    public void onResponse(Call<DefaultModel> call, Response<DefaultModel> response) {
                                        DefaultModel result = response.body();
                                        if (result.getStatus() == 1) {
                                            Toast.makeText(getApplicationContext(), "Registrasi berhasil, silakan login", Toast.LENGTH_SHORT).show();
                                            finish();
                                        } else {
                                            register.setLoading(false);
                                            new CustomSnackbar(binding.getRoot(), result.getMessage(), Snackbar.LENGTH_SHORT).setTextColor(Color.WHITE).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(final Call<DefaultModel> call, Throwable t) {
                                        if (count < 5) {
                                            call.clone().enqueue(this);
                                            count++;
                                        } else {
                                            register.setLoading(false);
                                            final Callback<DefaultModel> callback = this;
                                            new CustomSnackbar(binding.getRoot(), "Gagal, terjadi kesalahan dengan jaringan Anda", Snackbar.LENGTH_LONG, "Ulangi", new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    count = 0;
                                                    call.clone().enqueue(callback);
                                                }
                                            })
                                                    .setTextColor(Color.WHITE)
                                                    .show();
                                        }
                                    }
                                });
                            } else {
                                new CustomSnackbar(binding.getRoot(), "Re-Type Password tidak cocok", Snackbar.LENGTH_LONG).setTextColor(Color.WHITE).show();
                            }
                        }else {
                            new CustomSnackbar(binding.getRoot(), "Password minimal 6 karakter", Snackbar.LENGTH_LONG).setTextColor(Color.WHITE).show();
                        }
                    }else {
                        new CustomSnackbar(binding.getRoot(), "Gender tidak boleh kosong", Snackbar.LENGTH_LONG).setTextColor(Color.WHITE).show();
                    }
                    //Toast.makeText(view.getContext(), "RegisterViewModel berhasil "+register.getUsername()+register.getPassword()+gender, Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
