package id.kamal.panicbroadcast;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import id.kamal.panicbroadcast.databinding.FragmentContainerBinding;
import id.kamal.panicbroadcast.helper.PrefMgr;
import id.kamal.panicbroadcast.helper.api.RetrofitApi;
import id.kamal.panicbroadcast.helper.api.RetrofitApiSingleton;
import id.kamal.panicbroadcast.model.GetStatusBroadcastModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kamal on 20/12/16.
 */

public class FragmentContainer extends Fragment {

    View v;
    RetrofitApi api;
    int count = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentContainerBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_container, container, false);
        v = binding.getRoot();

        PrefMgr pref = PrefMgr.getInstance();
        pref.init(getContext());

        RetrofitApiSingleton.getInstance().init();
        api = RetrofitApiSingleton.getInstance().getApi();

        if(pref.isExist(PrefMgr.STATUS_BROADCAST)){
            if(pref.isBroadcasting()){
                FragmentTransaction transaction = getFragmentManager()
                        .beginTransaction();
                Bundle data = new Bundle();
                data.putString("id", pref.getBroadcastId());
                data.putString("jenis_bahaya", pref.getBroadcastJenisBahaya());
                data.putString("radius", pref.getBroadcastRadius());
                data.putString("lat", pref.getBroadcastLatitude());
                data.putString("lng", pref.getBroadcastLongitude());
                Broadcasting broadcasting = new Broadcasting();
                broadcasting.setArguments(data);
                transaction.replace(R.id.container_frame, broadcasting);
                transaction.commit();
            }else {
                FragmentTransaction transaction = getFragmentManager()
                        .beginTransaction();
                transaction.add(R.id.container_frame, new Broadcast());
                transaction.commit();
            }
        }else {
            FragmentTransaction transaction = getFragmentManager()
                    .beginTransaction();
            transaction.add(R.id.container_frame, new Broadcast());
            transaction.commit();

            Call<GetStatusBroadcastModel> getStatusBroadcast = api.getStatusBroadcast(pref.getIdUser());
            getStatusBroadcast.enqueue(new Callback<GetStatusBroadcastModel>() {
                @Override
                public void onResponse(Call<GetStatusBroadcastModel> call, Response<GetStatusBroadcastModel> response) {
                    count = 0;
                    GetStatusBroadcastModel result = response.body();
                    if(result.getStatus() == 1){
                        FragmentTransaction transaction = getFragmentManager()
                                .beginTransaction();
                        Bundle data = new Bundle();
                        data.putString("id", result.getData().getId());
                        data.putString("jenis_bahaya", result.getData().getJenisBahaya());
                        data.putString("radius", result.getData().getRadius());
                        data.putString("lat", result.getData().getLat());
                        data.putString("lng", result.getData().getLng());
                        Broadcasting broadcasting = new Broadcasting();
                        broadcasting.setArguments(data);
                        transaction.replace(R.id.container_frame, broadcasting);
                        transaction.commit();
                    }
                }

                @Override
                public void onFailure(Call<GetStatusBroadcastModel> call, Throwable t) {
                    if(count < 5){
                        call.clone().enqueue(this);
                        count++;
                    }
                }
            });
        }

        return v;
    }
}
