package id.kamal.panicbroadcast;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import id.kamal.panicbroadcast.helper.PrefMgr;


/**
 * Created by elmee on 09/03/2016.
 */
public class SplashScreen extends AppCompatActivity implements Animation.AnimationListener{

    Animation animation;

    private static int SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        setContentView(R.layout.splash_screen);
        //db = new DataBase(this);

        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.splash_screen);
        animation.setAnimationListener(this);
        ImageView logo = (ImageView) findViewById(R.id.logo);
        logo.startAnimation(animation);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                PrefMgr prefMgr = PrefMgr.getInstance();
                prefMgr.init(getApplicationContext());
                if(prefMgr.isExist(PrefMgr.ID_USER)){
                    startActivity(new Intent(SplashScreen.this, ActivityMain.class));
                }else {
                    startActivity(new Intent(SplashScreen.this, Login.class));
                }
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
