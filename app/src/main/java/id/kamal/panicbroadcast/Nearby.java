package id.kamal.panicbroadcast;

import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.Polygon;
import com.mapbox.mapboxsdk.annotations.PolygonOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationListener;
import com.mapbox.mapboxsdk.location.LocationServices;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import java.util.ArrayList;
import java.util.List;

import id.kamal.panicbroadcast.databinding.NearbyBinding;
import id.kamal.panicbroadcast.helper.CirclePolygon;
import id.kamal.panicbroadcast.helper.MainApplication;
import id.kamal.panicbroadcast.helper.PrefMgr;
import id.kamal.panicbroadcast.helper.api.RetrofitApi;
import id.kamal.panicbroadcast.helper.api.RetrofitApiSingleton;
import id.kamal.panicbroadcast.model.BroadcastModel;
import id.kamal.panicbroadcast.model.NearbyModel;
import id.kamal.panicbroadcast.viewmodel.NearbyViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kamal on 19/12/16.
 */

public class Nearby extends Fragment {

    View v;
    NearbyBinding binding;
    private LocationServices locationServices;
    private MapboxMap map;
    RetrofitApi api;
    PrefMgr pref;
    Location currentLoc;
    NearbyViewModel viewModel;
    private List<MarkerOptions> markerOptions = new ArrayList<>();
    private List<Marker> marker = new ArrayList<>();
    int count = 0;
    Polygon polygon;

    private static final int PERMISSIONS_LOCATION = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.nearby, container, false);
        viewModel = new NearbyViewModel();
        binding.setNearby(viewModel);
        v = binding.getRoot();
        /*((AppCompatActivity)getActivity()).setSupportActionBar(binding.toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        setHasOptionsMenu(false);*/

        locationServices = LocationServices.getLocationServices(getContext());
        RetrofitApiSingleton.getInstance().init();
        api = RetrofitApiSingleton.getInstance().getApi();
        pref = PrefMgr.getInstance();
        pref.init(getContext());

        binding.mapview.onCreate(savedInstanceState);
        binding.mapview.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {

                map = mapboxMap;
                //toggleGps(!map.isMyLocationEnabled());
                toggleGps(true);

            }
        });

        return v;
    }

    private void toggleGps(boolean enableGps) {
        if (enableGps) {
            // Check if user has granted location permission
            if (!locationServices.areLocationPermissionsGranted()) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{
                        android.Manifest.permission.ACCESS_COARSE_LOCATION,
                        android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_LOCATION);
            } else {
                enableLocation(true);
            }
        } else {
            enableLocation(false);
        }
    }

    private void enableLocation(boolean enabled) {
        if (enabled) {
            // If we have the last location of the user, we can move the camera to that position.
            Location lastLocation = locationServices.getLastLocation();
            currentLoc = lastLocation;
            if (lastLocation != null) {
                /*markerOptions = new MarkerOptions().position(new LatLng(lastLocation)).title("Lokasi").snippet("Lokasi Anda");
                if(marker != null) map.removeMarker(marker);
                marker = map.addMarker(markerOptions);*/
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lastLocation), 16));
                callNearby(currentLoc, "1000");
            }

            locationServices.addLocationListener(new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        // Move the map camera to where the user location is and then remove the
                        // listener so the camera isn't constantly updating when the user location
                        // changes. When the user disables and then enables the location again, this
                        // listener is registered again and will adjust the camera once again.
                        /*markerOptions = new MarkerOptions().position(new LatLng(location)).title("Lokasi").snippet("Lokasi Anda");
                        if(marker != null) map.removeMarker(marker);
                        marker = map.addMarker(markerOptions);*/
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location), 16));
                        locationServices.removeLocationListener(this);
                        currentLoc = location;
                        callNearby(currentLoc, "1000");
                        final String[] data = {"1 KM", "2 KM", "3 KM", "4 KM", "5 KM", "10 KM", "20 KM", "Others"};
                        final String[] value = {"1000", "2000", "3000", "4000", "5000", "10000", "20000"};

                        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainApplication.getContext(), android.R.layout.simple_spinner_item, data);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        binding.radius.setAdapter(adapter);
                        binding.radius.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                if(i != adapterView.getCount()-1){
                                    callNearby(currentLoc, value[i]);
                                }else{
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                    final View v = LayoutInflater.from(getContext()).inflate(R.layout.others, null);
                                    builder.setTitle("Others");
                                    builder.setMessage("Insert your own value in meters");
                                    builder.setView(v);
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            AppCompatEditText text = (AppCompatEditText)v.findViewById(R.id.text);
                                            if(text.getText().toString().length() > 0) {
                                                callNearby(currentLoc, text.getText().toString());
                                            }else{
                                                new AlertDialog.Builder(getContext()).setTitle("Error").setMessage("Value cannot be null").setPositiveButton("OK", null).show();
                                                dialogInterface.dismiss();
                                            }
                                        }
                                    });
                                    builder.setNegativeButton("Cancel", null);
                                    builder.show();
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }
                }
            });
            //floatingActionButton.setImageResource(R.drawable.ic_location_disabled_24dp);
        } else {
            //floatingActionButton.setImageResource(R.drawable.ic_my_location_24dp);
        }
        // Enable or disable the location layer on the map
        map.setMyLocationEnabled(enabled);

    }

    private void callNearby(Location loc, String radius){
        Call<NearbyModel> getNearby = api.getNearby(pref.getIdUser(), loc.getLatitude(), loc.getLongitude(), radius);
        viewModel.setLoading(true);
        getNearby.enqueue(new Callback<NearbyModel>() {
            @Override
            public void onResponse(Call<NearbyModel> call, Response<NearbyModel> response) {
                viewModel.setLoading(false);
                NearbyModel result = response.body();
                if(result.getStatus() == 1){
                    if(!marker.isEmpty()){
                        for(Marker mark : marker){
                            map.removeMarker(mark);
                        }
                        marker.clear();
                        markerOptions.clear();
                    }
                    for(NearbyModel.Datum data : result.getData()) {
                        LatLng lokasi = new LatLng(Double.parseDouble(data.getLat()), Double.parseDouble(data.getLng()));
                        MarkerOptions mo = new MarkerOptions().position(lokasi).title(data.getJenisBahayaNama()).snippet(data.getBroadcastMessage().length() > 0 ? data.getBroadcastMessage() : String.format(data.getDefaultMessage(), data.getLat()+", "+data.getLng()));
                        markerOptions.add(mo);
                        marker.add(map.addMarker(mo));
                        if(polygon != null){
                            polygon.remove();
                        }
                        CirclePolygon circlePolygon = new CirclePolygon(lokasi, Double.parseDouble(data.getRadius()));
                        polygon = map.addPolygon(new PolygonOptions().addAll(circlePolygon.polygonCircleForCoordinate()).fillColor(Color.RED).alpha(0.5f));
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(lokasi, 20));
                    }
                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                    builder.setTitle("Kesalahan");
                    builder.setMessage(result.getMessage());
                    builder.setPositiveButton("Tutup", null);
                    builder.show();
                }
            }

            @Override
            public void onFailure(final Call<NearbyModel> call, Throwable t) {
                if(count < 5){
                    call.clone().enqueue(this);
                    count++;
                }else {
                    try{
                        final Callback<NearbyModel> callback = this;
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainApplication.getContext());
                        builder.setTitle("Kesalahan");
                        builder.setMessage("Terjadi kesalahan dengan jaringan Anda");
                        builder.setPositiveButton("Ulangi", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                call.clone().enqueue(callback);
                                count = 0;
                            }
                        });
                        builder.setNegativeButton("Tutup", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                getActivity().finish();
                            }
                        });
                        builder.show();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        binding.mapview.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        binding.mapview.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.mapview.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding.mapview.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        binding.mapview.onSaveInstanceState(outState);
    }
}
