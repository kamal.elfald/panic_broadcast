package id.kamal.panicbroadcast.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by kamal on 28/12/16.
 */

public class NearbyModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("nama")
        @Expose
        private String nama;
        @SerializedName("lat")
        @Expose
        private String lat;
        @SerializedName("lng")
        @Expose
        private String lng;
        @SerializedName("broadcast_message")
        @Expose
        private String broadcastMessage;
        @SerializedName("default_message")
        @Expose
        private String defaultMessage;
        @SerializedName("radius")
        @Expose
        private String radius;
        @SerializedName("jenis_bahaya_id")
        @Expose
        private String jenisBahayaId;
        @SerializedName("jenis_bahaya_nama")
        @Expose
        private String jenisBahayaNama;
        @SerializedName("keterangan")
        @Expose
        private String keterangan;
        @SerializedName("broadcast_status")
        @Expose
        private String broadcastStatus;
        @SerializedName("distance")
        @Expose
        private String distance;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getBroadcastMessage() {
            return broadcastMessage;
        }

        public void setBroadcastMessage(String broadcastMessage) {
            this.broadcastMessage = broadcastMessage;
        }

        public String getDefaultMessage() {
            return defaultMessage;
        }

        public void setDefaultMessage(String defaultMessage) {
            this.defaultMessage = defaultMessage;
        }

        public String getRadius() {
            return radius;
        }

        public void setRadius(String radius) {
            this.radius = radius;
        }

        public String getJenisBahayaId() {
            return jenisBahayaId;
        }

        public void setJenisBahayaId(String jenisBahayaId) {
            this.jenisBahayaId = jenisBahayaId;
        }

        public String getJenisBahayaNama() {
            return jenisBahayaNama;
        }

        public void setJenisBahayaNama(String jenisBahayaNama) {
            this.jenisBahayaNama = jenisBahayaNama;
        }

        public String getKeterangan() {
            return keterangan;
        }

        public void setKeterangan(String keterangan) {
            this.keterangan = keterangan;
        }

        public String getBroadcastStatus() {
            return broadcastStatus;
        }

        public void setBroadcastStatus(String broadcastStatus) {
            this.broadcastStatus = broadcastStatus;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

    }

}
