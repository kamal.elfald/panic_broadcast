package id.kamal.panicbroadcast.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kamal on 24/11/16.
 */

public class LoginModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    /**
     *
     * @return
     * The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The message
     */
    public String getMessage() {
        return message;
    }

    /**
     *
     * @param message
     * The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     *
     * @return
     * The data
     */
    public Data getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("nama")
        @Expose
        private String nama;
        @SerializedName("alamat")
        @Expose
        private String alamat;
        @SerializedName("token")
        @Expose
        private String token;

        /**
         *
         * @return
         * The nama
         */
        public String getNama() {
            return nama;
        }

        /**
         *
         * @param nama
         * The nama
         */
        public void setNama(String nama) {
            this.nama = nama;
        }

        /**
         *
         * @return
         * The alamat
         */
        public String getAlamat() {
            return alamat;
        }

        /**
         *
         * @param alamat
         * The alamat
         */
        public void setAlamat(String alamat) {
            this.alamat = alamat;
        }

        /**
         *
         * @return
         * The token
         */
        public String getToken() {
            return token;
        }

        /**
         *
         * @param token
         * The token
         */
        public void setToken(String token) {
            this.token = token;
        }

    }

}
