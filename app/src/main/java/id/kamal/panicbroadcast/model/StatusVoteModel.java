package id.kamal.panicbroadcast.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kamal on 24/11/16.
 */

public class StatusVoteModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("status_vote")
        @Expose
        private Integer statusVote;
        @SerializedName("vote")
        @Expose
        private Integer vote;

        public Integer getStatusVote() {
            return statusVote;
        }

        public void setStatusVote(Integer statusVote) {
            this.statusVote = statusVote;
        }

        public Integer getVote() {
            return vote;
        }

        public void setVote(Integer vote) {
            this.vote = vote;
        }

    }

}
