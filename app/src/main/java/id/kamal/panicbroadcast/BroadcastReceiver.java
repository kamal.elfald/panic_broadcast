package id.kamal.panicbroadcast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.PolygonOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationListener;
import com.mapbox.mapboxsdk.location.LocationServices;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import org.json.JSONException;
import org.json.JSONObject;

import id.kamal.panicbroadcast.databinding.BroadcastReceiverBinding;
import id.kamal.panicbroadcast.helper.CirclePolygon;
import id.kamal.panicbroadcast.helper.PrefMgr;
import id.kamal.panicbroadcast.helper.api.RetrofitApi;
import id.kamal.panicbroadcast.helper.api.RetrofitApiSingleton;
import id.kamal.panicbroadcast.model.DefaultModel;
import id.kamal.panicbroadcast.model.StatusVoteModel;
import id.kamal.panicbroadcast.viewmodel.BroadcastReceiverViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kamal on 14/12/16.
 */

public class BroadcastReceiver extends AppCompatActivity {

    private LocationServices locationServices;
    private MapboxMap map;
    private Bundle bundle;
    private BroadcastReceiverBinding binding;
    private LatLng latLngDA;
    private double radius;
    private static final int PERMISSIONS_LOCATION = 0;

    private RetrofitApi api;
    private PrefMgr pref;
    private String id = "", nama = "", reputation = "";
    int count = 0, countStatusVote = 0;
    private FirebaseDatabase database;
    private DatabaseReference ref;
    private MarkerOptions markerOptions;
    private Marker marker;
    BroadcastReceiverViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
        binding = DataBindingUtil.setContentView(this, R.layout.broadcast_receiver);

        viewModel = new BroadcastReceiverViewModel();
        binding.setData(viewModel);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        RetrofitApiSingleton.getInstance().init();
        api = RetrofitApiSingleton.getInstance().getApi();
        pref = PrefMgr.getInstance();
        pref.init(this);

        bundle = getIntent().getExtras();

        try {
            if(bundle.getString("from").equals("notif")) {
                JSONObject data = new JSONObject(bundle.getString("data"));
                viewModel.setTitle(data.getString("title"));
                viewModel.setMessage(data.getString("message"));
                JSONObject payload = data.getJSONObject("payload");
                latLngDA = new LatLng(
                        Double.parseDouble(payload.getString("lat")),
                        Double.parseDouble(payload.getString("lng"))
                );
                radius = Double.parseDouble(payload.getString("radius"));
                id = payload.getString("id_user");
                viewModel.setNama(payload.getString("broadcaster"));
                viewModel.setReputation(payload.getString("reputation"));
                if(data.getString("judge").equals("hoax")||data.getString("judge").equals("real")){
                    binding.txtVoted.setVisibility(View.GONE);
                    viewModel.setModeHistory(true);
                }
            }else {
                viewModel.setTitle(bundle.getString("title"));
                viewModel.setMessage(bundle.getString("message"));
                latLngDA = new LatLng(
                        Double.parseDouble(bundle.getString("lat")),
                        Double.parseDouble(bundle.getString("lng"))
                );
                radius = Double.parseDouble(bundle.getString("radius"));
                id = bundle.getString("id");
                viewModel.setNama(bundle.getString("broadcaster"));
                viewModel.setReputation(bundle.getString("reputation"));
                viewModel.setModeHistory(bundle.getBoolean("from_history"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        locationServices = LocationServices.getLocationServices(this);

        binding.mapview.onCreate(savedInstanceState);
        binding.mapview.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {

                map = mapboxMap;
                //toggleGps(!map.isMyLocationEnabled());
                toggleGps(true);
                //map.setMyLocationEnabled(true);
            }
        });

        database = FirebaseDatabase.getInstance();

        binding.txtNama.setText(nama);
        binding.txtReputasi.setText(reputation);

        binding.btnNyata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vote(1);
            }
        });

        binding.btnHoax.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vote(0);
            }
        });

        viewModel.setVoted(false);
        if(!viewModel.isModeHistory()) {
            viewModel.setLoading(true);
            Call<StatusVoteModel> getStatusVote = api.getStatusVote(pref.getIdUser(), id);
            getStatusVote.enqueue(new Callback<StatusVoteModel>() {
                @Override
                public void onResponse(Call<StatusVoteModel> call, Response<StatusVoteModel> response) {
                    StatusVoteModel result = response.body();
                    viewModel.setLoading(false);
                    if (result.getStatus() == 1) {
                        StatusVoteModel.Data data = result.getData();
                        if (data.getStatusVote() == 1) {
                            if (data.getVote() == 1) {
                                binding.txtVoted.setText("Terima kasih. Anda telah memberi vote nyata (dukungan)");
                            } else {
                                binding.txtVoted.setText("Terima kasih. Anda telah memberi vote palsu (hoax)");
                            }
                            viewModel.setVoted(true);
                        } else {
                            viewModel.setVoted(false);
                        }
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(BroadcastReceiver.this);
                        builder.setTitle("Kesalahan");
                        builder.setMessage("Terjadi kesalahan dengan server");
                        builder.setPositiveButton("Tutup", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        });
                        builder.show();
                    }
                }

                @Override
                public void onFailure(final Call<StatusVoteModel> call, Throwable t) {
                    if (countStatusVote < 5) {
                        call.clone().enqueue(this);
                        countStatusVote++;
                    } else {
                        final Callback<StatusVoteModel> callback = this;
                        AlertDialog.Builder builder = new AlertDialog.Builder(BroadcastReceiver.this);
                        builder.setTitle("Kesalahan");
                        builder.setMessage("Terjadi kesalahan dengan jaringan Anda.");
                        builder.setPositiveButton("Ulangi", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                call.clone().enqueue(callback);
                                countStatusVote = 0;
                            }
                        });
                        builder.setNegativeButton("Tutup", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        });
                        builder.show();
                    }
                }
            });
        }

    }

    private void vote(final int vote){
        if(id.length() > 0) {
            viewModel.setLoading(true);
            Call<DefaultModel> broadcastVote = api.broadcastVote(pref.getIdUser(), id, vote);
            broadcastVote.enqueue(new Callback<DefaultModel>() {
                @Override
                public void onResponse(Call<DefaultModel> call, Response<DefaultModel> response) {
                    DefaultModel result = response.body();
                    /*AlertDialog.Builder builder = new AlertDialog.Builder(BroadcastReceiver.this);
                    builder.setTitle("Vote");
                    builder.setMessage(result.getMessage());
                    builder.setPositiveButton(result.getStatus() == 1 ? "Oke" : "Tutup", null);
                    builder.show();*/
                    viewModel.setLoading(false);
                    viewModel.setVoted(true);
                    binding.txtVoted.setText("Terima kasih. Anda telah memberi vote " + (vote == 1 ? "nyata (dukungan)" : "palsu (hoax)"));
                    ref = database.getReference("broadcast");
                    ref.child("broadcast-"+id).child(vote == 1 ? "support" : "hoax").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            int count = 0;
                            if(dataSnapshot.getValue() != null){
                                count = dataSnapshot.getValue(Integer.class);
                            }
                            ref.child("broadcast-"+id).child(vote == 1 ? "support" : "hoax").setValue(count+1);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.e("FIREBASE", "> "+databaseError);
                        }
                    });
                }

                @Override
                public void onFailure(final Call<DefaultModel> call, Throwable t) {
                    if(count < 5){
                        call.clone().enqueue(this);
                        count++;
                    }else {
                        final Callback<DefaultModel> callback = this;
                        AlertDialog.Builder builder = new AlertDialog.Builder(BroadcastReceiver.this);
                        builder.setTitle("Kesalahan");
                        builder.setMessage("Terjadi kesalahan dengan jaringan Anda");
                        builder.setPositiveButton("Ulangi", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                call.clone().enqueue(callback);
                            }
                        });
                        builder.setNegativeButton("Tutup", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        });
                        builder.show();
                    }
                }
            });
        }
    }

    private void toggleGps(boolean enableGps) {
        if (enableGps) {
            // Check if user has granted location permission
            if (!locationServices.areLocationPermissionsGranted()) {
                ActivityCompat.requestPermissions(this, new String[]{
                        android.Manifest.permission.ACCESS_COARSE_LOCATION,
                        android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_LOCATION);
            } else {
                enableLocation(true);
            }
        } else {
            enableLocation(false);
        }
    }

    private void enableLocation(boolean enabled) {
        if (enabled) {
            // If we have the last location of the user, we can move the camera to that position.
            Location lastLocation = locationServices.getLastLocation();
            if (lastLocation != null) {
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lastLocation), 16));
            }

            locationServices.addLocationListener(new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        // Move the map camera to where the user location is and then remove the
                        // listener so the camera isn't constantly updating when the user location
                        // changes. When the user disables and then enables the location again, this
                        // listener is registered again and will adjust the camera once again.
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location), 16));
                        locationServices.removeLocationListener(this);
                    }
                }
            });
            //floatingActionButton.setImageResource(R.drawable.ic_location_disabled_24dp);
        } else {
            //floatingActionButton.setImageResource(R.drawable.ic_my_location_24dp);
        }
        // Enable or disable the location layer on the map
        map.setMyLocationEnabled(enabled);
        markerOptions = new MarkerOptions().position(latLngDA).title(viewModel.getTitle()).snippet(viewModel.getMessage());
        marker = map.addMarker(markerOptions);
        CirclePolygon polygon = new CirclePolygon(latLngDA, radius);
        map.addPolygon(new PolygonOptions().addAll(polygon.polygonCircleForCoordinate()).fillColor(Color.RED).alpha(0.5f));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngDA, 16));
    }

    @Override
    public void onResume() {
        super.onResume();
        binding.mapview.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        binding.mapview.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.mapview.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding.mapview.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        binding.mapview.onSaveInstanceState(outState);
    }

}
