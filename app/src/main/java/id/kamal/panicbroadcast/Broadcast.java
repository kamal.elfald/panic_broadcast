package id.kamal.panicbroadcast;

import android.Manifest;
import android.animation.Animator;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationListener;
import com.mapbox.mapboxsdk.location.LocationServices;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import java.util.ArrayList;
import java.util.List;

import id.kamal.panicbroadcast.databinding.BroadcastBinding;
import id.kamal.panicbroadcast.helper.ApiInteractorImpl;
import id.kamal.panicbroadcast.helper.CustomSnackbar;
import id.kamal.panicbroadcast.helper.PrefMgr;
import id.kamal.panicbroadcast.helper.api.RetrofitApi;
import id.kamal.panicbroadcast.helper.api.RetrofitApiSingleton;
import id.kamal.panicbroadcast.model.BroadcastModel;
import id.kamal.panicbroadcast.model.DefaultModel;
import id.kamal.panicbroadcast.model.JenisBahayaModel;
import id.kamal.panicbroadcast.viewmodel.JenisBahayaViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;

import static android.app.Activity.RESULT_OK;

/**
 * Created by kamal on 29/11/16.
 */

public class Broadcast extends Fragment {

    private CompositeSubscription subscription = new CompositeSubscription();
    BroadcastBinding binding;
    int i = 0;
    private LocationServices locationServices;
    private MapboxMap map;
    MarkerOptions markerOptions;
    Marker marker;
    View v;
    Location currLocation;
    RetrofitApi api;
    PrefMgr pref;
    private int count = 0, countBroadcast = 0;
    String radius = "", jenisBahaya = "", jenisBahayaText = "";
    boolean broadcasting = false;

    private static final int PERMISSIONS_LOCATION = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.broadcast, container, false);
        v = binding.getRoot();

        /*((AppCompatActivity)getActivity()).setSupportActionBar(binding.toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        binding.toolbar.setOverflowIcon(ContextCompat.getDrawable(getContext(),R.drawable.ic_menu));*/
        pref = PrefMgr.getInstance();
        pref.init(v.getContext());

        if(pref.isExist(PrefMgr.STATUS_BROADCAST)){
            if(pref.isBroadcasting()){
                FragmentTransaction transaction = getFragmentManager()
                        .beginTransaction();
                Bundle data = new Bundle();
                data.putString("id", pref.getBroadcastId());
                data.putString("jenis_bahaya", pref.getBroadcastJenisBahaya());
                data.putString("radius", pref.getBroadcastRadius());
                data.putString("lat", pref.getBroadcastLatitude());
                data.putString("lng", pref.getBroadcastLongitude());
                Broadcasting broadcasting = new Broadcasting();
                broadcasting.setArguments(data);
                transaction.replace(R.id.container_frame, broadcasting);
                transaction.commit();
            }
        }

        RetrofitApiSingleton.getInstance().init();
        api = RetrofitApiSingleton.getInstance().getApi();

        JenisBahayaViewModel jenisBahayaViewModel = new JenisBahayaViewModel(new ApiInteractorImpl(), AndroidSchedulers.mainThread());
        showLoading();
        subscription.add(jenisBahayaViewModel.getJenisBahaya()
        .subscribe(new Observer<JenisBahayaModel>() {
            @Override
            public void onCompleted() {
                hideLoading();
            }

            @Override
            public void onError(Throwable e) {
                hideLoading();
                showSnack(e.getMessage());
            }

            @Override
            public void onNext(JenisBahayaModel jenisBahayaModel) {
                List<String> data = new ArrayList<String>();
                for(JenisBahayaModel.Datum isi : jenisBahayaModel.getData()){
                    data.add(isi.getNama());
                }
                updateUi(data, jenisBahayaModel.getData());
            }
        }));

        binding.fab.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final Handler handler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        if(i >= 0) {
                            binding.progress.setProgress(i++);
                            handler.postDelayed(this, 20);
                            if(i >= 100) {
                                if(!broadcasting) {
                                    broadcast();
                                    broadcasting = true;
                                }
                            }
                        }else if(i == -1 || i >= 100){
                            binding.progress.setProgress(0);
                        }
                    }
                };
                switch(motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        i = 0;
                        /*binding.fab.setShowProgressBackground(true);
                        binding.fab.setIndeterminate(false);*/
                        handler.postDelayed(runnable, 20);
                        binding.fab.setPressed(true);
                        // PRESSED
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        handler.removeCallbacks(runnable);
                        binding.progress.setProgress(0);
                        i = -1;
                        binding.fab.setPressed(false);
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        locationServices = LocationServices.getLocationServices(getContext());

        binding.mapview.onCreate(savedInstanceState);
        binding.mapview.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {

                map = mapboxMap;
                //toggleGps(!map.isMyLocationEnabled());
                toggleGps(true);
            }
        });

        binding.mapview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (currLocation != null) {
                        Bundle data = new Bundle();
                        data.putDouble("Lat", currLocation.getLatitude());
                        data.putDouble("Lng", currLocation.getLongitude());
                        startActivityForResult(new Intent(getContext(), ManualLocate.class).putExtras(data), 607);
                    }
                }
                return true;
            }
        });

        return v;
    }

    private void broadcast(){
        if(radius.length() > 0 && jenisBahaya.length() > 0 && jenisBahayaText.length() > 0) {
            final ProgressDialog loading = new ProgressDialog(getContext());
            loading.setMessage("Menyebarkan peringatan . . . . ");
            loading.setCancelable(false);
            loading.show();
            final Call<BroadcastModel> broadcast = api.broadcast(pref.getIdUser(), currLocation.getLatitude(), currLocation.getLongitude(), radius, jenisBahaya, "0");
            broadcast.enqueue(new Callback<BroadcastModel>() {
                @Override
                public void onResponse(Call<BroadcastModel> call, Response<BroadcastModel> response) {
                    countBroadcast = 0;
                    loading.dismiss();
                    BroadcastModel result = response.body();
                    if(result.getStatus() == 1){
                        Bundle data = new Bundle();
                        data.putString("id", String.valueOf(result.getData().getId()));
                        data.putString("jenis_bahaya", jenisBahayaText);
                        data.putString("radius", radius);
                        data.putString("lat", String.valueOf(currLocation.getLatitude()));
                        data.putString("lng", String.valueOf(currLocation.getLongitude()));
                        /*Intent intent = new Intent(getContext(), Broadcasting.class);
                        intent.putExtras(data);
                        startActivity(intent);*/

                        pref.saveStatusBroadcast(true);
                        pref.saveBroadcastId(String.valueOf(result.getData().getId()));
                        pref.saveBroadcastJenisBahaya(jenisBahayaText);
                        pref.saveBroadcastRadius(radius);
                        pref.saveBroadcastLat(String.valueOf(currLocation.getLatitude()));
                        pref.saveBroadcastLongitude(String.valueOf(currLocation.getLongitude()));

                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        Broadcasting broadcasting = new Broadcasting();
                        broadcasting.setArguments(data);
                        transaction.replace(R.id.container_frame, broadcasting);
                        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                        //transaction.addToBackStack(null);
                        transaction.commit();
                    }else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                        builder.setTitle("Kesalahan");
                        builder.setMessage(result.getMessage());
                        builder.setPositiveButton("Tutup", null);
                        builder.show();
                    }
                }

                @Override
                public void onFailure(final Call<BroadcastModel> call, Throwable t) {
                    if(countBroadcast < 5){
                        call.clone().enqueue(this);
                        countBroadcast++;
                    }else {
                        try{
                            final Callback<BroadcastModel> callback = this;
                            AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                            builder.setTitle("Kesalahan");
                            builder.setMessage("Terjadi kesalahan dengan jaringan Anda");
                            builder.setPositiveButton("Ulangi", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    call.clone().enqueue(callback);
                                    countBroadcast = 0;
                                }
                            });
                            builder.setNegativeButton("Tutup", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    loading.dismiss();
                                    getActivity().finish();
                                }
                            });
                            builder.show();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }

    /*private void animateRevealColorFromCoordinates(int x, int y) {
        float finalRadius = (float) Math.hypot(v.getWidth(), v.getHeight());

        Animator anim = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            anim = ViewAnimationUtils.createCircularReveal(v, x, y, 0, finalRadius);
        }
        v.setBackgroundColor(color);
        anim.start();
    }*/

    private void updateUi(List<String> data, final List<JenisBahayaModel.Datum> isi) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.jenisBahaya.setAdapter(adapter);
        binding.jenisBahaya.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                radius = isi.get(i).getDefaultRadius();
                jenisBahaya = isi.get(i).getId();
                jenisBahayaText = isi.get(i).getNama();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void showLoading(){
        binding.loading.setVisibility(View.VISIBLE);
        binding.jenisBahaya.setVisibility(View.GONE);
    }

    private void hideLoading(){
        binding.loading.setVisibility(View.GONE);
        binding.jenisBahaya.setVisibility(View.VISIBLE);
    }

    private void showSnack(String message) {
        Snackbar.make(binding.jenisBahaya, message, Snackbar.LENGTH_SHORT).setActionTextColor(Color.WHITE).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding.mapview.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        binding.mapview.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        binding.mapview.onResume();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.mapview.onLowMemory();
    }

    private void toggleGps(boolean enableGps) {
        if (enableGps) {
            // Check if user has granted location permission
            if (!locationServices.areLocationPermissionsGranted()) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_LOCATION);
            } else {
                enableLocation(true);
            }
        } else {
            enableLocation(false);
        }
    }

    private void enableLocation(boolean enabled) {
        if (enabled) {
            // If we have the last location of the user, we can move the camera to that position.
            Location lastLocation = locationServices.getLastLocation();
            currLocation = lastLocation;
            if (lastLocation != null) {
                markerOptions = new MarkerOptions().position(new LatLng(lastLocation)).title("Lokasi").snippet("Lokasi Anda");
                if(marker != null) map.removeMarker(marker);
                marker = map.addMarker(markerOptions);
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lastLocation), 16));
                updateLocation(pref.getIdUser(), lastLocation);
            }

            locationServices.addLocationListener(new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        // Move the map camera to where the user location is and then remove the
                        // listener so the camera isn't constantly updating when the user location
                        // changes. When the user disables and then enables the location again, this
                        // listener is registered again and will adjust the camera once again.markerOptions = new MarkerOptions().position(new LatLng(location)).title("Lokasi").snippet("Lokasi Anda");
                        if(marker != null) map.removeMarker(marker);
                        //marker = map.addMarker(markerOptions);
                        currLocation = location;
                        markerOptions = new MarkerOptions().position(new LatLng(location)).title("Lokasi").snippet("Lokasi Anda");
                        if(marker != null) map.removeMarker(marker);
                        marker = map.addMarker(markerOptions);
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location), 16));
                        locationServices.removeLocationListener(this);
                        updateLocation(pref.getIdUser(), location);
                    }
                }
            });
            //floatingActionButton.setImageResource(R.drawable.ic_location_disabled_24dp);
        } else {
            //floatingActionButton.setImageResource(R.drawable.ic_my_location_24dp);
        }
        // Enable or disable the location layer on the map
        //map.setMyLocationEnabled(enabled);
    }

    private void updateLocation(String token, Location location){
        Call<DefaultModel> updateLocation = api.updateLocation(token, String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
        updateLocation.enqueue(new Callback<DefaultModel>() {
            @Override
            public void onResponse(Call<DefaultModel> call, Response<DefaultModel> response) {
                count = 0;
                DefaultModel result = response.body();
                Log.i("Update Lokasi", "> "+result.getMessage());
            }

            @Override
            public void onFailure(final Call<DefaultModel> call, Throwable t) {
                if(count < 5){
                    count++;
                    call.clone().enqueue(this);
                }else {
                    /*final Callback<DefaultModel> callback = this;
                    new CustomSnackbar(binding.getRoot(), "Terjadi kesalahan dengan jaringan Anda", Snackbar.LENGTH_LONG,
                            "Ulangi", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            count = 0;
                            call.clone().enqueue(callback);
                        }
                    }).setTextColor(Color.WHITE).show();*/
                    Log.i("Update Lokasi", "GAGAL");
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                enableLocation(true);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            switch (requestCode){
                case 607:
                    markerOptions = new MarkerOptions().position(new LatLng(data.getExtras().getDouble("Lat"), data.getExtras().getDouble("Lng"))).title("Lokasi").snippet("Lokasi Anda");
                    currLocation.setLatitude(data.getExtras().getDouble("Lat"));
                    currLocation.setLongitude(data.getExtras().getDouble("Lng"));
                    if(marker != null) map.removeMarker(marker);
                    marker = map.addMarker(markerOptions);
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(data.getExtras().getDouble("Lat"), data.getExtras().getDouble("Lng")), 16));
                    //Toast.makeText(getContext(), "Lat : " + data.getExtras().getDouble("Lat") + "Lng : " + data.getExtras().getDouble("Lng"), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }


}
