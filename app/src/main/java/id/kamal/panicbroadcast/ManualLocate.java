package id.kamal.panicbroadcast;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PointF;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import id.kamal.panicbroadcast.databinding.ManualLocateBinding;

/**
 * Created by kamal on 03/12/16.
 */

public class ManualLocate extends AppCompatActivity {

    ManualLocateBinding binding;
    Bundle data;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
        binding = DataBindingUtil.setContentView(this, R.layout.manual_locate);
        binding.pilihLokasi.setVisibility(View.GONE);
        binding.mapview.onCreate(savedInstanceState);
        data = getIntent().getExtras();
        binding.mapview.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final MapboxMap mapboxMap) {

                // Customize map with markers, polylines, etc.
                mapboxMap.setMyLocationEnabled(true);
                mapboxMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(data.getDouble("Lat"), data.getDouble("Lng")), 16));
                Toast.makeText(ManualLocate.this, "Lat : " + data.getDouble("Lat") + "Lng : " + data.getDouble("Lng"), Toast.LENGTH_SHORT).show();
                mapboxMap.setOnCameraChangeListener(new MapboxMap.OnCameraChangeListener() {
                    @Override
                    public void onCameraChange(final CameraPosition position) {
                        //final LatLng posisi = mapboxMap.getProjection().fromScreenLocation(new PointF(binding.pin.getLeft() + (binding.pin.getWidth() / 2), binding.pin.getBottom() / 2));
                        //final LatLng posisi = position.target;
                        binding.pilihLokasi.setVisibility(View.VISIBLE);
                        //Toast.makeText(ManualLocate.this, "Lat : " + position.target.getLatitude() + "Lng : " + position.target.getLongitude(), Toast.LENGTH_SHORT).show();

                        binding.pilihLokasi.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if(position != null){
                                    Bundle data = new Bundle();
                                    data.putDouble("Lat", position.target.getLatitude());
                                    data.putDouble("Lng", position.target.getLongitude());
                                    Intent intent = new Intent();
                                    intent.putExtras(data);
                                    setResult(RESULT_OK, intent);
                                    finish();
                                }
                            }
                        });
                    }
                });

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        binding.mapview.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        binding.mapview.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.mapview.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding.mapview.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        binding.mapview.onSaveInstanceState(outState);
    }

}
