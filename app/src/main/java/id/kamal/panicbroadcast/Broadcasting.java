package id.kamal.panicbroadcast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.PolygonOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationListener;
import com.mapbox.mapboxsdk.location.LocationServices;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import id.kamal.panicbroadcast.databinding.BroadcastingBinding;
import id.kamal.panicbroadcast.helper.CirclePolygon;
import id.kamal.panicbroadcast.helper.PrefMgr;
import id.kamal.panicbroadcast.helper.api.RetrofitApi;
import id.kamal.panicbroadcast.helper.api.RetrofitApiSingleton;
import id.kamal.panicbroadcast.model.BroadcastModel;
import id.kamal.panicbroadcast.model.DefaultModel;
import id.kamal.panicbroadcast.viewmodel.BroadcastingViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kamal on 19/12/16.
 */

public class Broadcasting extends Fragment {

    private BroadcastingBinding binding;
    private View v;
    private LocationServices locationServices;
    private MapboxMap map;
    private MarkerOptions markerOptions;
    private Marker marker;

    private static final int PERMISSIONS_LOCATION = 0;

    private FirebaseDatabase database;
    private DatabaseReference ref;
    private RetrofitApi api;
    private int count = 0;
    private PrefMgr pref;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.broadcasting, container, false);
        v = binding.getRoot();

        final Bundle data = getArguments();

        final BroadcastingViewModel viewModel = new BroadcastingViewModel();
        viewModel.setJenisBahaya(data.getString("jenis_bahaya"));
        viewModel.setRadius(data.getString("radius"));
        viewModel.setSupportVote("0");
        viewModel.setHoaxVote("0");

        binding.setBroadcasting(viewModel);

        locationServices = LocationServices.getLocationServices(getContext());

        binding.mapview.onCreate(savedInstanceState);
        binding.mapview.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {

                map = mapboxMap;
                //toggleGps(!map.isMyLocationEnabled());
                LatLng lokasi = new LatLng(Double.parseDouble(data.getString("lat")), Double.parseDouble(data.getString("lng")));
                markerOptions = new MarkerOptions().position(lokasi).title(data.getString("jenis_bahaya")).snippet("Lokasi Bahaya");
                marker = map.addMarker(markerOptions);
                CirclePolygon polygon = new CirclePolygon(lokasi, Double.parseDouble(data.getString("radius")));
                map.addPolygon(new PolygonOptions().addAll(polygon.polygonCircleForCoordinate()).fillColor(Color.RED).alpha(0.5f));
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(lokasi, 20));
                toggleGps(true);
            }
        });

        database = FirebaseDatabase.getInstance();
        Log.e("FIREBASE", "> id broadcast "+data.getString("id"));
        ref = database.getReference("broadcast").child("broadcast-"+data.getString("id"));
        ref.child("support").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                viewModel.setSupportVote(dataSnapshot.getValue(Integer.class) == null ? "0" : String.valueOf(dataSnapshot.getValue(Integer.class)));
                Log.e("FIREBASE", "> support "+dataSnapshot.getValue(Integer.class)+" broadcast-"+data.getString("id") );
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("FIREBASE", "> "+databaseError);
            }
        });
        ref.child("hoax").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                viewModel.setHoaxVote(dataSnapshot.getValue(Integer.class) == null ? "0" : String.valueOf(dataSnapshot.getValue(Integer.class)));
                Log.e("FIREBASE", "> hoax "+dataSnapshot.getValue(Integer.class)+" broadcast-"+data.getString("id"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("FIREBASE", "> "+databaseError);
            }
        });

        RetrofitApiSingleton.getInstance().init();
        api = RetrofitApiSingleton.getInstance().getApi();
        pref = PrefMgr.getInstance();
        pref.init(getContext());

        binding.btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewModel.setLoading(true);
                Call<DefaultModel> stopBroadcast = api.stopBroadcast(pref.getIdUser(), data.getString("id"));
                stopBroadcast.enqueue(new Callback<DefaultModel>() {
                    @Override
                    public void onResponse(Call<DefaultModel> call, Response<DefaultModel> response) {
                        count = 0;
                        viewModel.setLoading(false);
                        DefaultModel result = response.body();
                        if(result.getStatus() == 1){
                            FragmentTransaction transaction = getFragmentManager().beginTransaction();
                            transaction.replace(R.id.container_frame, new Broadcast());
                            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                            //transaction.addToBackStack(null);
                            transaction.commit();
                            pref.saveStatusBroadcast(false);
                        }else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                            builder.setTitle("Kesalahan");
                            builder.setMessage(result.getMessage());
                            builder.setPositiveButton("Tutup", null);
                            builder.show();
                        }
                    }

                    @Override
                    public void onFailure(final Call<DefaultModel> call, Throwable t) {
                        if(count < 5){
                            call.clone().enqueue(this);
                            count++;
                        }else {
                            try {
                                final Callback<DefaultModel> callback = this;
                                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                                builder.setTitle("Kesalahan");
                                builder.setMessage("Terjadi kesalahan dengan jaringan Anda");
                                builder.setPositiveButton("Ulangi", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        call.clone().enqueue(callback);
                                        count = 0;
                                    }
                                });
                                builder.setNegativeButton("Tutup", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        getActivity().finish();
                                    }
                                });
                                builder.show();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        });

        return v;
    }

    private void toggleGps(boolean enableGps) {
        if (enableGps) {
            // Check if user has granted location permission
            if (!locationServices.areLocationPermissionsGranted()) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{
                        android.Manifest.permission.ACCESS_COARSE_LOCATION,
                        android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_LOCATION);
            } else {
                enableLocation(true);
            }
        } else {
            enableLocation(false);
        }
    }

    private void enableLocation(boolean enabled) {
        if (enabled) {
            // If we have the last location of the user, we can move the camera to that position.
            Location lastLocation = locationServices.getLastLocation();
            if (lastLocation != null) {
                /*markerOptions = new MarkerOptions().position(new LatLng(lastLocation)).title("Lokasi").snippet("Lokasi Anda");
                if(marker != null) map.removeMarker(marker);
                marker = map.addMarker(markerOptions);*/
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lastLocation), 16));
            }

            locationServices.addLocationListener(new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        // Move the map camera to where the user location is and then remove the
                        // listener so the camera isn't constantly updating when the user location
                        // changes. When the user disables and then enables the location again, this
                        // listener is registered again and will adjust the camera once again.
                        /*markerOptions = new MarkerOptions().position(new LatLng(location)).title("Lokasi").snippet("Lokasi Anda");
                        if(marker != null) map.removeMarker(marker);
                        marker = map.addMarker(markerOptions);*/
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location), 16));
                        locationServices.removeLocationListener(this);
                    }
                }
            });
            //floatingActionButton.setImageResource(R.drawable.ic_location_disabled_24dp);
        } else {
            //floatingActionButton.setImageResource(R.drawable.ic_my_location_24dp);
        }
        // Enable or disable the location layer on the map
        map.setMyLocationEnabled(enabled);
    }

    @Override
    public void onResume() {
        super.onResume();
        binding.mapview.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        binding.mapview.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.mapview.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding.mapview.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        binding.mapview.onSaveInstanceState(outState);
    }

}
