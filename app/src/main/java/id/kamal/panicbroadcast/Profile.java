package id.kamal.panicbroadcast;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;

import java.util.ArrayList;
import java.util.List;

import id.kamal.panicbroadcast.databinding.BroadcastListBinding;
import id.kamal.panicbroadcast.databinding.ProfileBinding;
import id.kamal.panicbroadcast.helper.MainApplication;
import id.kamal.panicbroadcast.helper.PrefMgr;
import id.kamal.panicbroadcast.helper.api.RetrofitApi;
import id.kamal.panicbroadcast.helper.api.RetrofitApiSingleton;
import id.kamal.panicbroadcast.model.BroadcastReceivedModel;
import id.kamal.panicbroadcast.model.GetBroadcastModel;
import id.kamal.panicbroadcast.model.ProfileModel;
import id.kamal.panicbroadcast.viewmodel.BroadcastListViewModel;
import id.kamal.panicbroadcast.viewmodel.ProfileViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kamal on 15/12/16.
 */

public class Profile extends Fragment {

    private ProfileBinding binding;
    private RetrofitApi api;
    private PrefMgr prefMgr;
    private int count = 0, countList = 0;
    ProfileViewModel profile;
    View v;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getActivity().getWindow().setStatusBarColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        }*/

        RetrofitApiSingleton.getInstance().init();
        api = RetrofitApiSingleton.getInstance().getApi();
        prefMgr = PrefMgr.getInstance();
        prefMgr.init(getContext());

        binding = DataBindingUtil.inflate(inflater, R.layout.profile, container, false);
        v = binding.getRoot();

        profile = new ProfileViewModel();
        binding.setProfile(profile);

        /*((AppCompatActivity)getActivity()).setSupportActionBar(binding.toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);*/

        profile.setLoading(true);
        Call<ProfileModel> getProfile = api.getProfile(prefMgr.getIdUser());
        getProfile.enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                profile.setLoading(false);
                count = 0;
                ProfileModel result = response.body();
                if(result.getStatus() == 1){
                    ProfileModel.Data data = result.getData();
                    profile.setNama(data.getNama());
                    profile.setAlamat(data.getAlamat());
                    profile.setEmail(data.getEmail());
                    profile.setReputation("Reputasi : "+data.getReputation());

                    String nama = "";
                    String[] namaArr = data.getNama().split(" ");
                    for(String name : namaArr){
                        nama += name.substring(0,1);
                    }

                    TextDrawable drawable = TextDrawable.builder()
                            .beginConfig()
                                .textColor(ContextCompat.getColor(MainApplication.getContext(), R.color.colorAccent))
                                .useFont(Typeface.createFromAsset(MainApplication.getContext().getAssets(), "font/Raleway-Bold.otf"))
                                .fontSize(35)
                                .toUpperCase()
                            .endConfig()
                            .buildRound(nama, ContextCompat.getColor(MainApplication.getContext(), R.color.colorPrimary));
                    binding.imgProfile.setImageDrawable(drawable);
                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainApplication.getContext());
                    builder.setTitle("Kesalahan");
                    builder.setMessage(result.getMessage());
                    builder.setPositiveButton("Tutup", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            getActivity().finish();
                        }
                    });
                    builder.show();
                }
            }

            @Override
            public void onFailure(final Call<ProfileModel> call, Throwable t) {
                if(count < 5){
                    call.clone().enqueue(this);
                    count++;
                }else {
                    try {
                        final Callback<ProfileModel> callback = this;
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle("Kesalahan");
                        builder.setMessage("Terjadi kesalahan dengan jaringan Anda!");
                        builder.setPositiveButton("Coba Lagi", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                count = 0;
                                call.clone().enqueue(callback);
                            }
                        });
                        builder.setNegativeButton("Tutup", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                getActivity().finish();
                            }
                        });
                        builder.show();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });

        broadcastList();

        binding.btnBroadcastList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                broadcastList();
            }
        });

        binding.btnBroadcastReceived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                broadcastReceived();
            }
        });

        return v;
    }

    private void broadcastList(){
        profile.setListLoading(true);
        Call<GetBroadcastModel> getBroadcast = api.getBroadcast(prefMgr.getIdUser());
        getBroadcast.enqueue(new Callback<GetBroadcastModel>() {
            @Override
            public void onResponse(Call<GetBroadcastModel> call, Response<GetBroadcastModel> response) {
                profile.setListLoading(false);
                countList = 0;
                final GetBroadcastModel result = response.body();
                if(result.getStatus() == 1) {
                    /*List<String> data = new ArrayList<String>();
                    for(GetBroadcastModel.Datum isi : result.getData()){
                        data.add(isi.getMessage());
                    }*/
                    binding.btnBroadcastList.setTextColor(ContextCompat.getColor(v.getContext(), R.color.colorAccent));
                    binding.btnBroadcastReceived.setTextColor(ContextCompat.getColor(v.getContext(), android.R.color.black));
                    BroadcastListAdapter adapter = new BroadcastListAdapter(getContext(), result.getData());
                    binding.listview.setAdapter(adapter);
                    binding.listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            Bundle bundle = new Bundle();
                            bundle.putString("from", "profile");
                            bundle.putString("id", String.valueOf(result.getData().get(i).getId()));
                            bundle.putString("title", result.getData().get(i).getJenisBahaya());
                            bundle.putString("message", result.getData().get(i).getMessage());
                            bundle.putString("lat", String.valueOf(result.getData().get(i).getLat()));
                            bundle.putString("lng", String.valueOf(result.getData().get(i).getLng()));
                            bundle.putString("radius", String.valueOf(result.getData().get(i).getRadius()));
                            bundle.putBoolean("from_history", true);
                            Intent intent = new Intent(getContext(), BroadcastReceiver.class);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    });
                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Kesalahan");
                    builder.setMessage(result.getMessage());
                    builder.setPositiveButton("Tutup", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            getActivity().finish();
                        }
                    });
                    builder.show();
                }
            }

            @Override
            public void onFailure(final Call<GetBroadcastModel> call, Throwable t) {
                if(countList < 5){
                    call.clone().enqueue(this);
                    countList++;
                }else {
                    final Callback<GetBroadcastModel> callback = this;
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Kesalahan");
                    builder.setMessage("Terjadi kesalahan dengan jaringan Anda!");
                    builder.setPositiveButton("Coba Lagi", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            countList = 0;
                            call.clone().enqueue(callback);
                        }
                    });
                    builder.setNegativeButton("Tutup", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            getActivity().finish();
                        }
                    });
                    builder.show();
                }
            }
        });
    }

    private void broadcastReceived(){
        profile.setListLoading(true);
        Call<BroadcastReceivedModel> getBroadcastReceived = api.getBroadcastReceived(prefMgr.getIdUser());
        getBroadcastReceived.enqueue(new Callback<BroadcastReceivedModel>() {
            @Override
            public void onResponse(Call<BroadcastReceivedModel> call, Response<BroadcastReceivedModel> response) {
                profile.setListLoading(false);
                countList = 0;
                final BroadcastReceivedModel result = response.body();
                if(result.getStatus() == 1) {
                    List<String> data = new ArrayList<String>();
                    for(BroadcastReceivedModel.Datum isi : result.getData()){
                        data.add(isi.getMessage());
                    }
                    binding.btnBroadcastList.setTextColor(ContextCompat.getColor(v.getContext(), android.R.color.black));
                    binding.btnBroadcastReceived.setTextColor(ContextCompat.getColor(v.getContext(), R.color.colorAccent));
                    BroadcastListAdapter adapter = new BroadcastListAdapter(getContext(), result.getData());
                    //ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, data);
                    binding.listview.setAdapter(adapter);
                    binding.listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            Bundle bundle = new Bundle();
                            bundle.putString("from", "profile");
                            bundle.putString("id", String.valueOf(result.getData().get(i).getId()));
                            bundle.putString("title", result.getData().get(i).getJenisBahaya());
                            bundle.putString("message", result.getData().get(i).getMessage());
                            bundle.putString("lat", String.valueOf(result.getData().get(i).getLat()));
                            bundle.putString("lng", String.valueOf(result.getData().get(i).getLng()));
                            bundle.putString("radius", String.valueOf(result.getData().get(i).getRadius()));
                            bundle.putString("nama", result.getData().get(i).getNama());
                            bundle.putString("reputation", result.getData().get(i).getReputation());
                            bundle.putBoolean("from_history", false);
                            Intent intent = new Intent(getContext(), BroadcastReceiver.class);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    });
                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Kesalahan");
                    builder.setMessage(result.getMessage());
                    builder.setPositiveButton("Tutup", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            getActivity().finish();
                        }
                    });
                    builder.show();
                }
            }

            @Override
            public void onFailure(final Call<BroadcastReceivedModel> call, Throwable t) {
                if(countList < 5){
                    call.clone().enqueue(this);
                    countList++;
                }else {
                    final Callback<BroadcastReceivedModel> callback = this;
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Kesalahan");
                    builder.setMessage("Terjadi kesalahan dengan jaringan Anda!");
                    builder.setPositiveButton("Coba Lagi", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            countList = 0;
                            call.clone().enqueue(callback);
                        }
                    });
                    builder.setNegativeButton("Tutup", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            getActivity().finish();
                        }
                    });
                    builder.show();
                }
            }
        });
    }

    public class BroadcastListAdapter<T> extends BaseAdapter{
        Context context;
        List<T> data;
        boolean status;

        public BroadcastListAdapter(Context context, List<T> data) {
            this.context = context;
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int i) {
            return data.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            BroadcastListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.broadcast_list, viewGroup, false);
            BroadcastListViewModel viewModel = new BroadcastListViewModel();

            View v = binding.getRoot();

            if(data.get(i) instanceof GetBroadcastModel.Datum){
                List<GetBroadcastModel.Datum> isi = (List<GetBroadcastModel.Datum>)data;
                viewModel.setTitle(isi.get(i).getMessage());
                viewModel.setWilayah("Wilayah : "+isi.get(i).getMessage().split("wilayah")[1]);
            }else if(data.get(i) instanceof BroadcastReceivedModel.Datum){
                List<BroadcastReceivedModel.Datum> isi = (List<BroadcastReceivedModel.Datum>)data;
                viewModel.setTitle(isi.get(i).getMessage());
                viewModel.setWilayah("Wilayah : "+isi.get(i).getMessage().split("wilayah")[1]);
            }

            binding.setData(viewModel);

            return v;
        }
    }

}
