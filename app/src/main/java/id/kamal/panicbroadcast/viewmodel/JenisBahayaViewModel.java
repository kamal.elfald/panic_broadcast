package id.kamal.panicbroadcast.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import id.kamal.panicbroadcast.helper.ApiInteractor;
import id.kamal.panicbroadcast.model.JenisBahayaModel;
import rx.Observable;
import rx.Scheduler;

/**
 * Created by kamal on 30/11/16.
 */

public class JenisBahayaViewModel extends BaseObservable {

    private ApiInteractor interactor;
    private Scheduler scheduler;

    public JenisBahayaViewModel(ApiInteractor interactor, Scheduler scheduler) {
        this.interactor = interactor;
        this.scheduler = scheduler;
    }

    public Observable<JenisBahayaModel> getJenisBahaya() {
        return interactor.getJenisBahaya().observeOn(scheduler);
    }

}
