package id.kamal.panicbroadcast.viewmodel;

import android.databinding.BindingAdapter;
import android.widget.TextView;

import id.kamal.panicbroadcast.helper.CustomFontFamily;

/**
 * Created by kamal on 24/11/16.
 */

public class FontBinding {

    @BindingAdapter({"bind:font"})
    public static void setFont(TextView textView, String fontName) {
        textView.setTypeface(CustomFontFamily.getInstance().getFont(fontName));
    }

}
