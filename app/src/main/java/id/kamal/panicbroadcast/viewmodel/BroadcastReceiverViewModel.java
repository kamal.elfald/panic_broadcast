package id.kamal.panicbroadcast.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.android.databinding.library.baseAdapters.BR;

/**
 * Created by kamal on 1/17/17.
 */

public class BroadcastReceiverViewModel extends BaseObservable {

    @Bindable
    String title, message, nama, reputation;
    @Bindable
    boolean modeHistory, loading, voted;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        notifyPropertyChanged(BR.title);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
        notifyPropertyChanged(BR.message);
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
        notifyPropertyChanged(BR.nama);
    }

    public String getReputation() {
        return reputation;
    }

    public void setReputation(String reputation) {
        this.reputation = reputation;
        notifyPropertyChanged(BR.reputation);
    }

    public boolean isModeHistory() {
        return modeHistory;
    }

    public void setModeHistory(boolean modeHistory) {
        this.modeHistory = modeHistory;
        notifyPropertyChanged(BR.modeHistory);
    }

    public boolean isLoading() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
        notifyPropertyChanged(BR.loading);
    }

    public boolean isVoted() {
        return voted;
    }

    public void setVoted(boolean voted) {
        this.voted = voted;
        notifyPropertyChanged(BR.voted);
    }
}