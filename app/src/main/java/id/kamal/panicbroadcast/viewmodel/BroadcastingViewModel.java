package id.kamal.panicbroadcast.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.android.databinding.library.baseAdapters.BR;

/**
 * Created by kamal on 19/12/16.
 */

public class BroadcastingViewModel extends BaseObservable{

    private String jenisBahaya, radius, supportVote, hoaxVote;
    private boolean loading;

    public BroadcastingViewModel() {
    }

    @Bindable
    public String getJenisBahaya() {
        return jenisBahaya;
    }

    public void setJenisBahaya(String jenisBahaya) {
        this.jenisBahaya = jenisBahaya;
        notifyPropertyChanged(BR.jenisBahaya);
    }

    @Bindable
    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
        notifyPropertyChanged(BR.radius);
    }

    @Bindable
    public String getSupportVote() {
        return supportVote;
    }

    public void setSupportVote(String supportVote) {
        this.supportVote = supportVote;
        notifyPropertyChanged(BR.supportVote);
    }

    @Bindable
    public String getHoaxVote() {
        return hoaxVote;
    }

    public void setHoaxVote(String hoaxVote) {
        this.hoaxVote = hoaxVote;
        notifyPropertyChanged(BR.hoaxVote);
    }

    @Bindable
    public boolean isLoading() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
        notifyPropertyChanged(BR.loading);
    }
}
