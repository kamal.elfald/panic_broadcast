package id.kamal.panicbroadcast.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.android.databinding.library.baseAdapters.BR;

/**
 * Created by kamal on 03/01/17.
 */

public class BroadcastListViewModel extends BaseObservable {

    @Bindable
    String title, wilayah;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        notifyPropertyChanged(BR.title);
    }

    public String getWilayah() {
        return wilayah;
    }

    public void setWilayah(String wilayah) {
        this.wilayah = wilayah;
        notifyPropertyChanged(BR.wilayah);
    }
}
