package id.kamal.panicbroadcast.viewmodel;

import android.app.Activity;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.databinding.library.baseAdapters.BR;

import id.kamal.panicbroadcast.ActivityMain;
import id.kamal.panicbroadcast.helper.CustomSnackbar;
import id.kamal.panicbroadcast.helper.PrefMgr;
import id.kamal.panicbroadcast.helper.api.RetrofitApi;
import id.kamal.panicbroadcast.helper.api.RetrofitApiSingleton;
import id.kamal.panicbroadcast.model.LoginModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kamal on 24/11/16.
 */

public class LoginViewModel extends BaseObservable{

    public String username, password;
    public boolean loading;
    int count = 0;

    public LoginViewModel() {
    }

    @Bindable
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Bindable
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Bindable
    public boolean isLoading() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
        notifyPropertyChanged(BR.loading);
    }

    public void onLogin(final View view, LoginViewModel login){
        if(login.getUsername() != null && login.getPassword() != null){
            RetrofitApiSingleton.getInstance().init();;
            RetrofitApi api = RetrofitApiSingleton.getInstance().getApi();

            PrefMgr pref = PrefMgr.getInstance();

            if(login.getPassword().length() > 5) {
                setLoading(true);
                Call<LoginModel> apiLogin = api.login(login.getUsername(), login.getPassword(), pref.getTokenFirebase());
                apiLogin.enqueue(new Callback<LoginModel>() {
                    @Override
                    public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                        //setLoading(false);
                        count = 0;
                        if (response.isSuccessful()) {
                            LoginModel model = response.body();
                            if (model.getStatus() == 1) {
                                PrefMgr prefMgr = PrefMgr.getInstance();
                                prefMgr.saveIdUser(model.getData().getToken());
                                view.getContext().startActivity(new Intent(view.getContext(), ActivityMain.class));
                                ((Activity) view.getContext()).finish();
                            } else {
                                Snackbar snackbar = Snackbar.make(view, "Gagal, Username atau password salah", Snackbar.LENGTH_LONG);
                                TextView tv = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.WHITE);
                                snackbar.show();
                                setLoading(false);
                            }
                        } else {
                            Snackbar snackbar = Snackbar.make(view, "Gagal, terjadi kesalahan dengan jaringan Anda", Snackbar.LENGTH_LONG);
                            TextView tv = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.WHITE);
                            snackbar.show();
                            setLoading(false);
                        }
                    }

                    @Override
                    public void onFailure(final Call<LoginModel> call, Throwable t) {
                        if (count < 5) {
                            call.clone().enqueue(this);
                            count++;
                        } else {
                            setLoading(false);
                            final Callback<LoginModel> callback = this;
                            Snackbar snackbar = Snackbar.make(view, "Gagal, terjadi kesalahan dengan jaringan Anda", Snackbar.LENGTH_LONG);
                            TextView tv = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.WHITE);
                            snackbar.setAction("Ulangi", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    count = 0;
                                    call.clone().enqueue(callback);
                                }
                            }).show();
                        }
                    }
                });
            }else {
                new CustomSnackbar(view, "Password minimal 6 karakter", Snackbar.LENGTH_SHORT).setTextColor(Color.WHITE).show();
            }

        }else {
            /*Snackbar snackbar = Snackbar.make(view, "Username dan Password tidak valid", Snackbar.LENGTH_LONG);
            TextView tv = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
            snackbar.show();*/
            new CustomSnackbar(view, "Username dan Password tidak valid", Snackbar.LENGTH_SHORT).setTextColor(Color.WHITE).show();
        }
    }

    public void register(View view){
        Intent intent = new Intent(view.getContext(), id.kamal.panicbroadcast.Register.class);
        view.getContext().startActivity(intent);

    }
}
