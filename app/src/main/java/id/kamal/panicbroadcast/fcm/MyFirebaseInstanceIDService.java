package id.kamal.panicbroadcast.fcm;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import android.util.Log;

import id.kamal.panicbroadcast.helper.MainApplication;
import id.kamal.panicbroadcast.helper.PrefMgr;
import id.kamal.panicbroadcast.helper.api.RetrofitApi;
import id.kamal.panicbroadcast.helper.api.RetrofitApiSingleton;
import id.kamal.panicbroadcast.model.DefaultModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kamal on 05/12/16.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    PrefMgr prefMgr;
    int count = 0;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("Firebase", "Refreshed token: " + refreshedToken);

        prefMgr = PrefMgr.getInstance();
        prefMgr.saveTokenFirebase(refreshedToken);
        prefMgr.saveStatusToken(false);
        if(prefMgr.isExist(PrefMgr.ID_USER)){
            sendRegistrationToServer(prefMgr.getIdUser(), refreshedToken);
        }
    }

    private void sendRegistrationToServer(String tokenUser, String refreshedToken){
        RetrofitApi api;
        RetrofitApiSingleton.getInstance().init();
        api = RetrofitApiSingleton.getInstance().getApi();

        Call<DefaultModel> updateUser = api.updateUser(tokenUser, refreshedToken);
        updateUser.enqueue(new Callback<DefaultModel>() {
            @Override
            public void onResponse(Call<DefaultModel> call, Response<DefaultModel> response) {
                count = 0;
                DefaultModel result = response.body();
                Log.i("SEND TOKEN", result.getMessage());
                prefMgr.saveStatusToken(true);
            }

            @Override
            public void onFailure(Call<DefaultModel> call, Throwable t) {
                if(count < 5){
                    count++;
                    call.clone().enqueue(this);
                }else {
                    Log.i("SEND TOKEN", "GAGAL");
                    count = 0;
                }
            }
        });
    }
}
